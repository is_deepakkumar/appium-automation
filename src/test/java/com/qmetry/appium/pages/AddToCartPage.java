package com.qmetry.appium.pages;

import org.hamcrest.Matchers;

import com.qmetry.appium.utility.Utility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class AddToCartPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator="verify.addToCart.productTitle")
	private QAFWebElement productTitle;
	@FindBy(locator="verify.addToCart.sizeBtn")
	private QAFWebElement sizeBtn;
	@FindBy(locator="verify.addToCart.size")
	private QAFWebElement size;
	@FindBy(locator="verify.addToCart.popUp")
	private QAFWebElement popUp;
	@FindBy(locator="verify.addToCart.addToCart")
	private QAFWebElement addToCartBtn;
	@FindBy(locator="verify.addToCart.tostMsg")
	private QAFWebElement tostMsg;
	@FindBy(locator="verify.addToCart.viewcart")
	private QAFWebElement viewcart;
	@FindBy(locator="verify.addToCart.inCart")
	private QAFWebElement inCart;
	
	public QAFWebElement getProductTitle() {
		return productTitle;
	}
	public QAFWebElement getSizeBtn() {
		return sizeBtn;
	}
	public QAFWebElement getSize() {
		return size;
	}
	public QAFWebElement getPopUp() {
		return popUp;
	}
	public QAFWebElement getAddToCartBtn() {
		return addToCartBtn;
	}
	public QAFWebElement getTostMsg() {
		return tostMsg;
	}
	public QAFWebElement getViewcart() {
		return viewcart;
	}
	public QAFWebElement getInCart() {
		return inCart;
	}
	
	NavigationPage navigatePage=new NavigationPage();
	
	
	public void cart()
	{
		Utility.scrollVertical();
		Reporter.log("Selected Product is :");
		getSizeBtn().click();
		getSize().click();
		getPopUp().click();
		Utility.scrollVertical();	
		getAddToCartBtn().click();
		Utility.scrollVertical();
		Reporter.log("product added to cart");
		navigatePage.getTshirtDetails().waitForVisible(5000);
		Validator.verifyThat("Product Details page is displayed", navigatePage.getTshirtDetails().getText(),Matchers.containsString(ConfigurationManager.getBundle().getPropertyValue("product.details.page")));
		getViewcart().click();
		getInCart().waitForVisible(10000);
		
	}
	public void addToCart()
	{ 
		//Product one
		
		navigatePage.nagivateToProduct();
		navigatePage.getTshirt1().waitForVisible(10000);
		navigatePage.getTshirt1().click();
		Reporter.log("Adding to cart= "+getProductTitle().getText());
		String Tshirt1 =getProductTitle().getText();
		cart();
		Validator.verifyThat("Product is Displayed IN CART", Tshirt1,Matchers.containsString(getInCart().getText()));
		
		
		//Product two
		navigatePage.getHamBurgerMenu().click();
		navigatePage.nagivateToProduct();
		navigatePage.getTshirt2().click();
		Reporter.log("Adding to cart= "+getProductTitle().getText());
		String Tshirt2 =getProductTitle().getText();
		cart();
		Validator.verifyThat("Product is Displayed IN CART", Tshirt2 ,Matchers.containsString(getInCart().getText()));
		
		
		//Product three
		/*navigatePage.getHamBurgerMenu().click();
		navigatePage.nagivateToProduct();
		Utility.scrollVertical();	
		navigatePage.getTshirt3().click();
		Reporter.log("Adding to cart= "+getProductTitle().getText());
		String Tshirt3 =getProductTitle().getText();
		cart();
		Validator.verifyThat("Product is Displayed IN CART", Tshirt3,Matchers.containsString(getInCart().getText()));*/
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

	
}
