package com.qmetry.appium.pages;

import com.qmetry.appium.beans.RegistrationDataBeans;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.testng.report.Report;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import org.hamcrest.Matchers;

public class RegistrationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	RegistrationDataBeans formData = new RegistrationDataBeans();
	@FindBy(locator = "registration.skip.signIn.btn")
	private QAFWebElement registrationSkipSigninBtn;
	@FindBy(locator = "registration.hamBurger.btn")
	private QAFWebElement registrationHamburgerBtn;
	@FindBy(locator = "registration.hello.signIn.link")
	private QAFWebElement registrationHelloSigninLink;
	@FindBy(locator = "registration.create.acount.radioBtn")
	private QAFWebElement registrationCreateAcountRadiobtn;
	@FindBy(locator = "registration.name.text")
	private QAFWebElement registrationNameText;
	@FindBy(locator = "registration.phoneNumber")
	private QAFWebElement registrationPhonenumber;
	@FindBy(locator = "registration.email")
	private QAFWebElement registrationEmail;
	@FindBy(locator = "registration.set.password")
	private QAFWebElement registrationSetPassword;
	@FindBy(locator = "registration.continue.btn")
	private QAFWebElement registrationContinueBtn;
	@FindBy(locator = "registration.verify.clickRandom")
	private QAFWebElement registrationVerifyClickrandom;
	/*@FindBy(locator = "registration.verification.page")
	private QAFWebElement registrationVerificationPage;*/
	@FindBy(locator="registration.verification.page")
	private QAFWebElement mobileNoVerificationPage;
	@FindBy(locator = "registration.error.msg")
	private QAFWebElement registrationErrorMsg;
	@FindBy(locator = "registration.welcome.lbl")
	private QAFWebElement registrationWelcomeLbl;
	
	@FindBy(locator="reg.signin.txt")
	private QAFWebElement signin;


	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
	}

	public QAFWebElement getRegistrationSkipSigninBtn() {
		return registrationSkipSigninBtn;
	}

	public QAFWebElement getRegistrationHamburgerBtn() {
		return registrationHamburgerBtn;
	}

	public QAFWebElement getRegistrationHelloSigninLink() {
		return registrationHelloSigninLink;
	}

	public QAFWebElement getRegistrationCreateAcountRadiobtn() {
		return registrationCreateAcountRadiobtn;
	}

	public QAFWebElement getRegistrationNameText() {
		return registrationNameText;
	}

	public QAFWebElement getRegistrationPhonenumber() {
		return registrationPhonenumber;
	}

	public QAFWebElement getRegistrationEmail() {
		return registrationEmail;
	}

	public QAFWebElement getRegistrationSetPassword() {
		return registrationSetPassword;
	}

	public QAFWebElement getRegistrationContinueBtn() {
		return registrationContinueBtn;
	}

	public QAFWebElement getRegistrationVerifyClickrandom() {
		return registrationVerifyClickrandom;
	}



	/*public QAFWebElement getRegistrationVerificationPage() {
		return registrationVerificationPage;
	}*/
	
	public QAFWebElement getSignin() {
		return signin;
	}

	public QAFWebElement getMobileNoVerificationPage() {
		return mobileNoVerificationPage;
	}

	public QAFWebElement getRegistrationErrorMsg() {
		return registrationErrorMsg;
	}

	public QAFWebElement getRegistrationWelcomeLbl() {
		return registrationWelcomeLbl;
	}
	
	public void doRegistraion() {
		
		skipLogin();
		Validator.verifyTrue(getRegistrationHamburgerBtn().isDisplayed(), "hamburger menu is not present", "hamburger menu is present");
		getRegistrationHamburgerBtn().click();
		Validator.verifyTrue(getRegistrationHelloSigninLink().isPresent(), "SignIn link is Not present in left menu", "SignIn link is present in left menu");
		getRegistrationHelloSigninLink().click();
		Validator.verifyTrue(getRegistrationCreateAcountRadiobtn().isDisplayed(), "Create Account Btn is not present", "Create Account Btn is present");
		getRegistrationCreateAcountRadiobtn().click();
		Validator.verifyTrue(getRegistrationNameText().isPresent(), "Name Field is Not present", "Name Field is present");
		
		formData.fillRandomData();	
		
		getRegistrationNameText().sendKeys(formData.getName());
		Reporter.log(getRegistrationNameText().getText());
		Validator.verifyTrue(getRegistrationPhonenumber().isPresent(), "Phone Number Field is Not present", "Phone Number Field is present");
		getRegistrationPhonenumber().sendKeys(formData.getPhoneNumber());
		Reporter.log(getRegistrationPhonenumber().getText());
		Validator.verifyTrue(getRegistrationEmail().isPresent(), "Email Field is Not present", "Email Field is present");
		getRegistrationEmail().sendKeys(formData.getEmail());
		Reporter.log(getRegistrationEmail().getText());
		Validator.verifyTrue(getRegistrationSetPassword().isPresent(), "Password Field is Not present", "Password Field is present");
		getRegistrationSetPassword().sendKeys(formData.getPassword());
		Reporter.log(formData.getPassword());
		Validator.verifyTrue(getRegistrationContinueBtn().isPresent(), "Continue Btn is not present", "Continue Btn is present");
		getRegistrationWelcomeLbl().click();
		getRegistrationContinueBtn().click();
		Reporter.log("clicking on continue btn");
		getMobileNoVerificationPage().waitForVisible(10000);
		
		Validator.verifyThat("Mobile Number Verification page is displayed", getMobileNoVerificationPage().getText(),Matchers.containsString("Verify mobile number"));
		Reporter.log(getMobileNoVerificationPage().getText());
	}
	
	public void test()
	{
		skipLogin();
		Reporter.log(getSignin().getText());
		
	}
	public void skipLogin() {

		getRegistrationSkipSigninBtn().click();
	}
	
	public void doRegistraionWithExistingUser() {
		
		skipLogin();	
		getRegistrationHamburgerBtn().click();	
		getRegistrationHelloSigninLink().click();	
		getRegistrationCreateAcountRadiobtn().click();	
		
		formData.fillFromConfig("register.data");		
		getRegistrationNameText().sendKeys(formData.getName());
		Reporter.log(getRegistrationNameText().getText());		
		getRegistrationPhonenumber().sendKeys(formData.getPhoneNumber());
		Reporter.log(getRegistrationPhonenumber().getText());		
		getRegistrationEmail().sendKeys(formData.getEmail());
		Reporter.log(getRegistrationEmail().getText());		
		getRegistrationSetPassword().sendKeys(formData.getPassword());
		Reporter.log(formData.getPassword());		
		getRegistrationWelcomeLbl().click();
		getRegistrationContinueBtn().waitForVisible(5000);
		getRegistrationContinueBtn().click();			
		Reporter.log("clicking on continue btn");
		getRegistrationErrorMsg().waitForVisible(5000);
		//String text=driver.switchTo().alert().getText();
		System.out.println("Hi i text");
		//System.out.println(text);
		Reporter.log(getRegistrationErrorMsg().getText());
		Validator.verifyThat("you",Matchers.containsString("You indicated you are a new customer, but an account already exists with the mobile phone number"));
		//Reporter.log(getRegistrationErrorMsg().getText());
		
		//You indicated you are a new customer, but an account already exists with the mobile phone number XXXX
	}
	
}
