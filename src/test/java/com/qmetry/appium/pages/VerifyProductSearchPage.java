package com.qmetry.appium.pages;

import java.util.List;

import com.qmetry.appium.components.ProductSearchComponent;
import com.qmetry.appium.utility.Utility;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class VerifyProductSearchPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator="Verify.SearchProduct.searchBar")
	private QAFWebElement searchBar;
	@FindBy(locator="Verify.SearchProduct.displayedProduct")
	private QAFWebElement displayedProduct;
	@FindBy(locator="Verify.SearchProduct.home")
	private QAFWebElement home;
	
	@FindBy(locator="verify.search.filterDropDown")
	private QAFWebElement filterDropDown;
	@FindBy(locator="verify.search.format")
	private QAFWebElement format;
	@FindBy(locator="verify.search.paperback")
	private QAFWebElement paperback;
	
	@FindBy(locator="Verify.SearchProduct.searchBarText")
	private QAFWebElement searchBarText;
	
	@FindBy(locator="Verify.SearchProduct.content")
	private List<ProductSearchComponent> productContent;
	
	@FindBy(locator="verify.search.sortMenu")
	private QAFWebElement sortMenu;
	@FindBy(locator="verify.search.sortLowToHigh")
	private QAFWebElement sortLowToHigh;
	@FindBy(locator="verify.search.price")
	private List<QAFWebElement> price;
	
	public QAFWebElement getSearchBar() {
		return searchBar;
	}

	public QAFWebElement getDisplayedProduct() {
		return displayedProduct;
	}
	public QAFWebElement getHome() {
		return home;
	}
	public List<ProductSearchComponent> getProductContent() {
		return productContent;
	}
	
	public QAFWebElement getSearchBarText() {
		return searchBarText;
	}
	
	public QAFWebElement getFilterDropDown() {
		return filterDropDown;
	}
	public QAFWebElement getFormat() {
		return format;
	}
	public QAFWebElement getPaperback() {
		return paperback;
	}
	
	public QAFWebElement getSortMenu() {
		return sortMenu;
	}
	public QAFWebElement getSortLowToHigh() {
		return sortLowToHigh;
	}
	public List<QAFWebElement> getPrice() {
		return price;
	}
	
	
	public void search(String productName)
	{
		//getHome().click();
		
		getSearchBar().click();
		Validator.verifyTrue(getSearchBar().isPresent(), "Search Bar is Not present", "SearchBar is present");
		getSearchBar().sendKeys(productName+"\n");
		
		//Utility.scrollVertical();
		Reporter.log("Search Result");
		Utility.scrollVertical();
		//Utility.scrollVertical();
		//String[] split=productName.split(" ");
		for (int i=0;i<2;i++)
			{ 
			if(getProductContent().get(i).getProductDescription().getText().contains(productName)){
			Reporter.log(" Product Title=   " +getProductContent().get(i).getProductTitle().getText() +"  Product Description=  "+ getProductContent().get(i).getProductDescription().getText());
		}
			else
			{
				Reporter.log("Wrong Product on search "+" Product Title=   " +getProductContent().get(i).getProductTitle().getText() +"  Product Description=  "+ getProductContent().get(i).getProductDescription().getText());
			}
		}}
	
	public void searchFilter(String productName)
	{
		//getHome().click();
		
		getSearchBar().click();
		Validator.verifyTrue(getSearchBar().isPresent(), "Search Bar is Not present", "SearchBar is present");
		getSearchBar().sendKeys(productName+"\n");
		getFilterDropDown().waitForVisible(5000);
		getFilterDropDown().click();
		Utility.scrollVertical();
		getFormat().click();
		getPaperback().click();
		String edition=getPaperback().getText();
		driver.navigate().back();
		Utility.scrollVertical();
		//Utility.scrollVertical();
		for (int i=0;i<2;i++)
		{ 
			if(getProductContent().get(i).getEdition().getText().contains(edition)){
				Reporter.log(" Product Title=   " +getProductContent().get(i).getProductDescription().getText() +"  Format=  "+ getProductContent().get(i).getEdition().getText());
			}
			else
			{
				Reporter.log("Wrong Result on Filter:  Product Title=   " +getProductContent().get(i).getProductDescription().getText() +"  Format=  "+ getProductContent().get(i).getEdition().getText());
			}
		}
		
	}

	public void sortingFilter(String productName)
	{
		getSearchBar().click();
		Validator.verifyTrue(getSearchBar().isPresent(), "Search Bar is Not present", "SearchBar is present");
		getSearchBar().sendKeys(productName+"\n");
	
		getFilterDropDown().waitForVisible(5000);
		for(int i=0;i<2;i++)
		{
			Reporter.log("Before Sorting Price Was= "+ getPrice().get(i).getText());
		}
	
		//getFilterDropDown().click();
		getSortMenu().click();
		getSortLowToHigh().click();
		Reporter.log("Selecting Sorting Type");
		driver.navigate().back();
		for(int i=0;i<2;i++)
		{
			Reporter.log("After Sorting Price is= "+ getPrice().get(i).getText());
		}
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub
		
	}

}
