package com.qmetry.appium.pages;

import org.hamcrest.Matchers;
import com.qmetry.appium.utility.Utility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;


public class NavigationPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "navigate.products.hamBurger.btn")
	private QAFWebElement hamBurgerMenu;
	@FindBy(locator = "navigate.products.home")
	private QAFWebElement home;
	@FindBy(locator = "navigate.products.category")
	private QAFWebElement category;
	@FindBy(locator = "navigate.products.MensFashion")
	private QAFWebElement mensFashion;
	@FindBy(locator = "navigate.products.AmazonFashion")
	private QAFWebElement amazonFashion;
	@FindBy(locator = "navigate.products.men")
	private QAFWebElement men;
	@FindBy(locator = "navigate.products.clothing")
	private QAFWebElement clothing;
	@FindBy(locator = "navigate.products.tShirtsCatogry")
	private QAFWebElement tShirtsCatogry;
	@FindBy(locator = "navigate.products.tshirt1")
	private QAFWebElement tshirt1;
	@FindBy(locator = "navigate.products.tshirt2")
	private QAFWebElement tshirt2;
	@FindBy(locator = "navigate.products.tshirt3")
	private QAFWebElement tshirt3;
	@FindBy(locator = "navigate.products.tShirt.details")
	private QAFWebElement tshirtDetails;

	@FindBy(locator = "navigate.products.menclothing")
	private QAFWebElement menclothing;
	
	public QAFWebElement getMenClothing() {
		return menclothing;
	}
	
	public QAFWebElement getHamBurgerMenu() {
		return hamBurgerMenu;
	}

	public QAFWebElement getHome() {
		return home;
	}

	public QAFWebElement getCategory() {
		return category;
	}

	public QAFWebElement getMensFashion() {
		return mensFashion;
	}

	public QAFWebElement getAmazonFashion() {
		return amazonFashion;
	}

	public QAFWebElement getMen() {
		return men;
	}

	public QAFWebElement getClothing() {
		return clothing;
	}

	public QAFWebElement gettShirtsCatogry() {
		return tShirtsCatogry;
	}

	public QAFWebElement getTshirt1() {
		return tshirt1;
	}

	public QAFWebElement getTshirt2() {
		return tshirt2;
	}

	public QAFWebElement getTshirt3() {
		return tshirt3;
	}

	public QAFWebElement getTshirtDetails() {
		return tshirtDetails;
	}

	//static Utility scroll=new Utility();

	
	public void nagivateToProduct() {
		getCategory().waitForVisible(10000);
		getCategory().click();
		getMensFashion().waitForVisible(10000);
		getMensFashion().click();
		//Utility.scrollVertical();
		//Reporter.log("Selected Category is :" +getMensFashion().getText());
		getAmazonFashion().click();
		getMen().waitForVisible(5000);
		getMen().click();
		//getMenClothing().waitForVisible(15000);
		//getMenClothing().click();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		
		//getClothing().waitForVisible(10000);
		getClothing().click();
		//gettShirtsCatogry().waitForVisible(5000);
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		gettShirtsCatogry().click();

	}

	public void selectProducts() {

		// product one
		nagivateToProduct();
//		Utility.scrollVertical();
//		Utility.scrollVertical();
//		Utility.scrollVertical();
		//Utility.scrollVertical();
		//Utility.scrollVertical();
		//Utility.scrollVertical();
		getTshirt1().waitForVisible(15000);
		getTshirt1().click();	
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Utility.scrollVertical();
		Reporter.log("Selected Product is :");
		System.out.println(getTshirtDetails().getText());
		Validator.verifyThat("Product Details page is displayed", getTshirtDetails().getText(),
				Matchers.containsString(ConfigurationManager.getBundle().getPropertyValue("product.details.page")));

		// Product two
		/*getHamBurgerMenu().click();
		nagivateToProduct();
		getTshirt2().click();
		//Utility.scrollVertical();
		Reporter.log("Selected Product is :");
		Validator.verifyThat("Product Details page is displayed", getTshirtDetails().getText(),
				Matchers.containsString(ConfigurationManager.getBundle().getPropertyValue("product.details.page")));*/

		// Product three
		/*getHamBurgerMenu().click();
		nagivateToProduct();
		Utility.scrollVertical();
		getTshirt3().click();
		scroll.scrollVertical();
		Reporter.log("Selected Product is :");
		Validator.verifyThat("Product Details page is displayed", getTshirtDetails().getText(),
				Matchers.containsString(ConfigurationManager.getBundle().getPropertyValue("product.details.page")));*/
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

}
