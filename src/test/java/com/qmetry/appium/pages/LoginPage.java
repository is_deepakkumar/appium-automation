package com.qmetry.appium.pages;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;

public class LoginPage extends WebDriverBaseTestPage<WebDriverTestPage>{

	@FindBy(locator="login.signIn.btn")
	private QAFWebElement signInBtn;
	@FindBy(locator="login.mobileOrEmail.text")
	private QAFWebElement email;
	@FindBy(locator="login.continue.btn")
	private QAFWebElement continueBtn;
	@FindBy(locator="login.password.text")
	private QAFWebElement password;
	@FindBy(locator="login.submit.btn")
	private QAFWebElement submitBtn;
	@FindBy(locator="login.hamBurger.menu")
	private QAFWebElement hamBurgerMenu;
	@FindBy(locator="login.home.page")
	private QAFWebElement homePage;
	
	
	public QAFWebElement getSignInBtn() {
		return signInBtn;
	}
	public QAFWebElement getEmail() {
		return email;
	}
	public QAFWebElement getContinueBtn() {
		return continueBtn;
	}
	public QAFWebElement getPassword() {
		return password;
	}
	public QAFWebElement getSubmitBtn() {
		return submitBtn;
	}
	public QAFWebElement getHamBurgerMenu() {
		return hamBurgerMenu;
	}
	public QAFWebElement getHomePage() {
		return homePage;
	}

	public void signIn()
	{
		getSignInBtn().click();
		
		getEmail().sendKeys(ConfigurationManager.getBundle().getString("login.emailId"));
		getEmail().waitForVisible(10000);
		getContinueBtn().click();
		getPassword().sendKeys(ConfigurationManager.getBundle().getString("login.password"));
		getSubmitBtn().click();
		//getHamBurgerMenu().waitForVisible(10000);
		//getHamBurgerMenu().click();
		//Validator.verifyTrue(getHomePage().isPresent(), "Login Failed..! Home Page is Not Displayed", "Login Successfull..! Home Page is Displayed");

	}

	public void signin()
	{
		getSignInBtn().click();
		
		getEmail().sendKeys(ConfigurationManager.getBundle().getString("login.emailId"));
		getEmail().waitForVisible(10000);
		getContinueBtn().click();
		getPassword().sendKeys(ConfigurationManager.getBundle().getString("login.password"));
		getSubmitBtn().click();
		getHamBurgerMenu().waitForVisible(10000);
		getHamBurgerMenu().click();
		Validator.verifyTrue(getHomePage().isPresent(), "Login Failed..! Home Page is Not Displayed", "Login Successfull..! Home Page is Displayed");

	}
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		
	}
	

}
