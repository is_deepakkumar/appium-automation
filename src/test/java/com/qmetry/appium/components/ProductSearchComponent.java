package com.qmetry.appium.components;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class ProductSearchComponent extends QAFWebComponent {

	public ProductSearchComponent(String locator) {
		super(locator);		
	}
	
	@FindBy(locator="Verify.SearchProduct.title")
	private QAFWebElement productTitle;
	@FindBy(locator="Verify.SearchProduct.description")
	private QAFWebElement productDescription;
	@FindBy(locator="Verify.SearchProduct.edition")
	private QAFWebElement edition;
	
	public QAFWebElement getProductTitle() {
		return productTitle;
	}
	public QAFWebElement getProductDescription() {
		return productDescription;
	}
	public QAFWebElement getEdition() {
		return edition;
	}
	

}
