package com.qmetry.appium.utility;

import org.openqa.selenium.Dimension;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidTouchAction;
import io.appium.java_client.touch.offset.PointOption;


public class Utility{

	@SuppressWarnings("all")
	public static AndroidDriver<?> getDriver() {
		return (AndroidDriver<?>) new WebDriverTestBase().getDriver().getUnderLayingDriver();
	}  
 
	
	public  static void scrollVertical()
	{
		Dimension size = Utility.getDriver().manage().window().getSize();
		int startXpoint=size.getWidth()/2;
		int startYpoint=size.getHeight()/2;
		int endXpoint=startXpoint;
		int endYpoint= (int)(startYpoint * 0.30 ); 
		System.out.println(startXpoint+" : "+startYpoint+ " : "+endXpoint + " : "+endYpoint);
		AndroidTouchAction touch = new AndroidTouchAction(Utility.getDriver());
		touch.longPress(PointOption.point(startXpoint,startYpoint))
		.moveTo(PointOption.point(endXpoint, endYpoint))
		.release()
		.perform(); 
				
	}

}
