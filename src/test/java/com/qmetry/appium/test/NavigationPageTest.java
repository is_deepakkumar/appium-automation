package com.qmetry.appium.test;

import org.testng.annotations.Test;

import com.qmetry.appium.pages.LoginPage;
import com.qmetry.appium.pages.NavigationPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class NavigationPageTest extends WebDriverTestBase {
	
	@Test
	public void NavigateThroughProducts()
	{
		LoginPage login=new LoginPage();
		login.signIn(); 
		
		NavigationPage products=new NavigationPage();
		products.selectProducts();
	}

}
