package com.qmetry.appium.test;

import org.testng.annotations.Test;

import com.qmetry.appium.pages.AddToCartPage;
import com.qmetry.appium.pages.LoginPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class AddToCartPageTest extends WebDriverTestBase {

	@Test
	public void verifyAddToCart()
	{
		LoginPage login=new LoginPage();
		login.signin(); 
		
		AddToCartPage cart=new AddToCartPage();
		cart.addToCart();
	}
}
