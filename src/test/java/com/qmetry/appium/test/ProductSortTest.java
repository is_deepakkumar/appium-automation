package com.qmetry.appium.test;

import java.util.Map;

import org.testng.annotations.Test;

import com.qmetry.appium.pages.LoginPage;
import com.qmetry.appium.pages.VerifyProductSearchPage;
import com.qmetry.qaf.automation.testng.dataprovider.QAFDataProvider;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class ProductSortTest extends WebDriverTestBase {

	@QAFDataProvider(key="search.filter")
	@Test
	public void verifySortFilter(Map<String,String> data) {
		
		LoginPage login=new LoginPage();
		login.signIn(); 
		VerifyProductSearchPage sort=new VerifyProductSearchPage();
		sort.sortingFilter(data.get("productName"));
}
}