package com.qmetry.appium.beans;

import com.qmetry.qaf.automation.data.BaseFormDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class RegistrationDataBeans extends BaseFormDataBean {
	
	@Randomizer(length=6, type=RandomizerTypes.LETTERS_ONLY)
	private String name;
	
	@Randomizer(length=7,suffix="@infostretch.com")
	private String email;
	
	@Randomizer(length=10, type=RandomizerTypes.DIGITS_ONLY)
	private String phoneNumber;
	
	@Randomizer(length=8, type=RandomizerTypes.MIXED)
	private String password;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
